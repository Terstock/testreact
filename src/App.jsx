import Menu from "./components/Menu";
import Slider from "./components/Slider";
function App() {
  return (
    <>
      <Menu />
      <Slider />
    </>
  );
}

export default App;
