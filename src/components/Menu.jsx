import "./Menu.css";

function Menu() {
  return (
    <nav className="nav-menu">
      <a href="#" className="nav-menu-item">
        Home
      </a>
      <a href="#" className="nav-menu-item">
        About
      </a>
      <a href="#" className="nav-menu-item">
        Contacts
      </a>
      <a href="#" className="nav-menu-item">
        Catalog
      </a>
      <a href="#" className="nav-menu-item">
        Back
      </a>
    </nav>
  );
}

export default Menu;
