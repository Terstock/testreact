import SliderImage from "../assets/Photo.jpg";

function Slider() {
  return (
    <div className="slider">
      <img src={SliderImage} alt="" className="slide-slide" />
    </div>
  );
}

export default Slider;
